/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.quests;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import org.bukkit.inventory.ItemStack;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to store a "deliver-item"-Quest.
 * Created by tr808axm on 09.05.2016.
 */
public class ItemDeliveryQuest extends Quest {
    private final String title;
    private final BigDecimal reward;
    private final ItemStack item;

    public ItemDeliveryQuest(String title, BigDecimal reward, ItemStack item) {
        this.title = title;
        this.reward = reward;
        this.item = item;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public BigDecimal getReward() {
        return reward;
    }

    @Override
    public boolean submit(QuestPlayer player) {
        if (player.getBukkitPlayer().getInventory().getItemInHand() != null && player.getBukkitPlayer().getInventory().getItemInHand().equals(item)) {
            player.getBukkitPlayer().getInventory().setItemInHand(null);
            return true;
        }
        return false;
    }

    @Override
    public String[] asListStrings(int id) {
        String[] listStrings = new String[4];
        listStrings[0] = "";
        listStrings[1] = ChatUtil.VARIABLE + getTitle();
        listStrings[2] = ChatUtil.DETAILS + "ID: " + id;
        listStrings[3] = ChatUtil.DETAILS + "Deliver: " + item.toString(); //TODO add format
        return listStrings;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> questMap = new HashMap<>();
        questMap.put("reward", reward.toString());
        questMap.put("title", title);
        questMap.put("type", "DELIVER_ITEM");
        Map<String, Object> locationMap = item.serialize();
        questMap.put("item", locationMap);
        return questMap;
    }
}
