/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.quests.command;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.quests.*;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to parse and handle the /quests command.
 * Created by tr808axm on 10.04.2016.
 */
public class QuestsCommandExecutor implements CommandExecutor {
    private final QuestsMain plugin;
    private final Map<String, String> commands = new HashMap<>();

    public QuestsCommandExecutor(QuestsMain plugin) {
        this.plugin = plugin;
        commands.put("/quests [help]", "Show the help menu.");
        commands.put("/quests create <visit_location|deliver_item> <reward> <title...>", "Creates a new Quests with the specified type.");
        commands.put("/quests list", "List all quests.");
        commands.put("/quests my <active|done>", "List your active/done quests.");
        commands.put("/quests submit <ID>", "Submit something to a quest.");
        commands.put("/quests take <quest-name>", "Take a quest.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length >= 1) {
            if (args[0].equalsIgnoreCase("create")) {
                if (sender.isOp()) {
                    if (sender instanceof Player) {
                        if (args.length >= 2) {
                            if (args.length >= 4) {
                                String title = args[3];
                                for (int i = 4; i < args.length; i++) title += " " + args[i];
                                BigDecimal reward;
                                try {
                                    reward = new BigDecimal(args[2]);
                                } catch (NumberFormatException e) {
                                    plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + args[2] + ChatUtil.NEGATIVE + " isn't a valid number!", false);
                                    return true;
                                }
                                int id = -1;
                                if (args[1].equalsIgnoreCase("DELIVER_ITEM")) {
                                    if (((Player) sender).getInventory().getItemInHand() != null && ((Player) sender).getInventory().getItemInHand().getType() != Material.AIR) {
                                        id = plugin.getQuestManager().addQuest(new ItemDeliveryQuest(title, new BigDecimal(args[2]), ((Player) sender).getInventory().getItemInHand()));
                                    }
                                } else if (args[1].equalsIgnoreCase("VISIT_LOCATION")) {
                                    id = plugin.getQuestManager().addQuest(new LocationQuest(title, reward, ((Player) sender).getLocation(), 3));
                                } else {
                                    plugin.getChatUtil().send(sender, ChatUtil.INVALID_ARGUMENTS_MESSAGE, false);
                                    return true;
                                }
                                plugin.getChatUtil().send(sender, "A quest with the id " + ChatUtil.VARIABLE + id + ChatUtil.POSITIVE + " was created!", true);
                                return true;
                            }
                            plugin.getChatUtil().send(sender, ChatUtil.INVALID_ARGUMENTS_MESSAGE, false);
                            return true;
                        } else {
                            plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("/quests create", "/quests create <visit_location|deliver_item> <reward> <title...>", commands.get("/quests create <visit_location|deliver_item> <reward> <title...>")), true);
                            return true;
                        }
                    } else {
                        plugin.getChatUtil().send(sender, ChatUtil.NO_EXECUTION_FROM_CONSOLE, false);
                        return true;
                    }
                } else {
                    plugin.getChatUtil().send(sender, ChatUtil.NO_PERMISSION_MESSAGE, false);
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("help")) {
                plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("Commands", commands), true);
                return true;
            } else if (args[0].equalsIgnoreCase("list")) {
                plugin.getQuestManager().list(sender);
                return true;
            } else if (args[0].equalsIgnoreCase("my")) {
                if (sender instanceof Player) {
                    if (args.length >= 2) {
                        if (args[1].equalsIgnoreCase("active")) {
                            QuestPlayer questPlayer = plugin.getQuestManager().getQuestPlayer((Player) sender);
                            String[] messages;
                            if (questPlayer.getActiveQuestCount() == 0) {
                                messages = new String[2];
                                messages[0] = "----- Your active quests (" + questPlayer.getActiveQuestCount() + "/" + questPlayer.getActiveQuests().length + ")-----";
                                messages[1] = "" + ChatUtil.VARIABLE + ChatColor.ITALIC + "(none)";
                            } else {
                                messages = new String[1 + 4 * questPlayer.getActiveQuestCount()];
                                messages[0] = "----- Your active quests (" + questPlayer.getActiveQuestCount() + "/" + questPlayer.getActiveQuests().length + ")-----";
                                int i = 1;
                                Quest[] activeQuests = questPlayer.getActiveQuests();
                                for (Quest quest : activeQuests) {
                                    if (quest != null) {
                                        String[] questListStrings = quest.asListStrings(plugin.getQuestManager().getQuestIdentifier(quest));
                                        messages[i++] = questListStrings[0];
                                        messages[i++] = questListStrings[1];
                                        messages[i++] = questListStrings[2];
                                        messages[i++] = questListStrings[3];
                                    }
                                }
                            }
                            plugin.getChatUtil().send(sender, messages, true);
                            return true;
                        } else if (args[1].equalsIgnoreCase("done")) {
                            QuestPlayer questPlayer = plugin.getQuestManager().getQuestPlayer((Player) sender);
                            String[] messages;
                            Quest[] doneQuests = questPlayer.getDoneQuests();
                            if (doneQuests.length == 0) {
                                messages = new String[2];
                                messages[0] = "----- Your done quests (" + doneQuests.length + ")-----";
                                messages[1] = "" + ChatUtil.VARIABLE + ChatColor.ITALIC + "(none)";
                            } else {
                                messages = new String[1 + 4 * questPlayer.getDoneQuests().length];
                                messages[0] = "----- Your done quests (" + doneQuests.length + ")-----";
                                int i = 1;
                                for (Quest quest : doneQuests) {
                                    String[] questListStrings = quest.asListStrings(plugin.getQuestManager().getQuestIdentifier(quest));
                                    messages[i++] = questListStrings[0];
                                    messages[i++] = questListStrings[1];
                                    messages[i++] = questListStrings[2];
                                    messages[i++] = questListStrings[3];
                                }
                            }
                            plugin.getChatUtil().send(sender, messages, true);
                            return true;
                        }
                    }
                    plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("/quests my", "/quests my <active|done>", commands.get("/quests my <active|done>")), true);
                    return true;
                }
                plugin.getChatUtil().send(sender, ChatUtil.NO_EXECUTION_FROM_CONSOLE, false);
                return true;
            } else if (args[0].equalsIgnoreCase("submit")) {
                if (sender instanceof Player) {
                    if (args.length >= 2) {
                        Integer id;
                        try {
                            id = Integer.parseInt(args[1]);
                        } catch (NumberFormatException e) {
                            plugin.getChatUtil().send(sender, "'" + ChatUtil.VARIABLE + args[1] + ChatUtil.NEGATIVE + "' is not a valid id!", false);
                            return true;
                        }
                        Quest quest = plugin.getQuestManager().getQuest(id);
                        QuestPlayer questPlayer = plugin.getQuestManager().getQuestPlayer((Player) sender);
                        if (quest != null && questPlayer != null) {
                            if (questPlayer.submit(quest)) {
                                plugin.getChatUtil().send(sender, "Successfully submitted something to quest #" + id + "!", true);
                                return true;
                            } else {
                                plugin.getChatUtil().send(sender, "Submission to quest #" + id + " failed!", false);
                                return true;
                            }
                        }

                    } else {
                        plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("/quests submit", "/quests submit <ID>", commands.get("/quests submit <ID>")), true);
                        return true;
                    }
                }
                plugin.getChatUtil().send(sender, ChatUtil.NO_EXECUTION_FROM_CONSOLE, false);
                return true;
            } else if (args[0].equalsIgnoreCase("take")) {
                if (sender instanceof Player) {
                    if (args.length >= 2) {
                        Integer questIdentifier;
                        try {
                            questIdentifier = Integer.parseInt(args[1]);
                        } catch (NumberFormatException e) {
                            plugin.getChatUtil().send(sender, ChatUtil.VARIABLE + args[1] + ChatUtil.NEGATIVE + " isn't a valid number!", false);
                            return true;
                        }
                        Quest quest = plugin.getQuestManager().getQuest(questIdentifier);
                        if (quest != null) {
                            if (plugin.getQuestManager().getQuestPlayer((Player) sender).takeQuest(quest)) {
                                plugin.getChatUtil().send(sender, "Successfully took quest '" + ChatUtil.VARIABLE + quest.getTitle() + ChatUtil.POSITIVE + "'! Good Luck!", true);
                                return true;
                            } else {
                                plugin.getChatUtil().send(sender, "Could not take quest '" + ChatUtil.VARIABLE + quest.getTitle() + ChatUtil.NEGATIVE + "'.", false);
                                return true;
                            }
                        } else {
                            plugin.getChatUtil().send(sender, "There is no quest with the id '" + ChatUtil.VARIABLE + questIdentifier + ChatUtil.NEGATIVE + "'!", false);
                            return true;
                        }
                    }
                    plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("/quests take", "/quests take <quest-name>", commands.get("/quests take <quest-name>")), true);
                    return true;
                } else {
                    plugin.getChatUtil().send(sender, ChatUtil.NO_EXECUTION_FROM_CONSOLE, false);
                    return true;
                }
            }
            plugin.getChatUtil().send(sender, ChatUtil.INVALID_ARGUMENTS_MESSAGE, false);
            return true;
        }
        plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("Commands", commands), true);
        return true;
    }
}
