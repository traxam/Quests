/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.quests;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.pluginutils.ConfigurationLoader;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Used to manage Quests.
 * Created by tr808axm on 30.04.2016.
 */
public class QuestManager {
    private final QuestsMain plugin;
    private final ConfigurationLoader playerDataConfigurationLoader;
    private final ConfigurationLoader questsConfigurationLoader;
    private Map<Integer, Quest> questList;
    private final Map<UUID, QuestPlayer> playerList = new HashMap<>(); // Map as a shortcut
    private Integer lastIdentifier;
    private List<Map<?, ?>> __COMPAT_newIllegalQuests__;

    public QuestManager(QuestsMain plugin) throws IOException {
        this.plugin = plugin;
        lastIdentifier = 0;
        {
            questsConfigurationLoader = new ConfigurationLoader(plugin, "quests.yml");
            questsConfigurationLoader.addConfigurationReader(new ConfigurationLoader.ConfigurationReader() {
                @Override
                public void readConfig(FileConfiguration fileConfiguration) {
                    readQuestsConfig(fileConfiguration);
                }
            });
            questsConfigurationLoader.addConfigurationWriter(new ConfigurationLoader.ConfigurationWriter() {
                @Override
                public void writeConfig(FileConfiguration fileConfiguration) {
                    saveQuestConfig(fileConfiguration);
                }
            });
            questsConfigurationLoader.reload(true);
        }
        {
            playerDataConfigurationLoader = new ConfigurationLoader(plugin, "player-data.yml");
            playerDataConfigurationLoader.addConfigurationReader(new ConfigurationLoader.ConfigurationReader() {
                @Override
                public void readConfig(FileConfiguration fileConfiguration) {
                    readPlayerDataConfig(fileConfiguration);
                }
            });
            playerDataConfigurationLoader.addConfigurationWriter(new ConfigurationLoader.ConfigurationWriter() {
                @Override
                public void writeConfig(FileConfiguration fileConfiguration) {
                    savePlayerDataConfig(fileConfiguration);
                }
            });
            playerDataConfigurationLoader.reload(true);
        }

    }

    private void readPlayerDataConfig(FileConfiguration fileConfiguration) {
        plugin.getLogger().info("Reading player data from player-data.yml...");
        {
            ConfigurationSection playersConfigurationSection = fileConfiguration.getConfigurationSection("players");

            for (String uuidString : playersConfigurationSection.getValues(false).keySet()) {
                UUID uuid = UUID.fromString(uuidString);

                List<Integer> activeQuestIDs = playersConfigurationSection.getConfigurationSection(uuidString).getIntegerList("active-quests");
                Quest[] activeQuests = new Quest[activeQuestIDs.size() > 3 ? activeQuestIDs.size() : 3];
                {
                    int i = 0;
                    for (Integer activeQuestID : activeQuestIDs) {
                        activeQuests[i] = getQuest(activeQuestID);
                        i++;
                    }
                }

                List<Integer> doneQuestIDs = playersConfigurationSection.getConfigurationSection(uuidString).getIntegerList("done-quests");
                Quest[] doneQuests = new Quest[doneQuestIDs.size()];
                {
                    int i = 0;
                    for (Integer doneQuestID : doneQuestIDs) {
                        doneQuests[i] = getQuest(doneQuestID);
                        i++;
                    }
                }

                QuestPlayer questPlayer = new QuestPlayer(plugin, uuid, activeQuests, doneQuests);
                playerList.put(uuid, questPlayer);
            }
            plugin.getLogger().info("...found " + playerList.size() + " players!");
        }
    }

    private void readQuestsConfig(FileConfiguration fileConfiguration) {
        plugin.getLogger().info("Reading quests from quests.yml...");
        {
            questList = new HashMap<>();
            lastIdentifier = fileConfiguration.getInt("last-identifier");

            List<Map<?, ?>> questMapList = fileConfiguration.getMapList("quests");
            List<Map<?, ?>> newIllegalQuests = new ArrayList<>();
            for (Map<?, ?> questMap : questMapList) {
                Integer id = (Integer) questMap.get("id");
                if (id > lastIdentifier) lastIdentifier = id;
                BigDecimal reward = new BigDecimal((String) questMap.get("reward"));
                String title = (String) questMap.get("title");
                String questType = (String) questMap.get("type");
                if (questType.equalsIgnoreCase("DELIVER_ITEM")) {
                    @SuppressWarnings("unchecked") ItemStack item = ItemStack.deserialize((Map<String, Object>) questMap.get("item"));
                    questList.put(id, new ItemDeliveryQuest(title, reward, item));

                } else if (questType.equalsIgnoreCase("VISIT_LOCATION")) { //TODO move this. Quests should be easy-to-use and extensible
                    try {
                        @SuppressWarnings("unchecked") Location location = Location.deserialize((Map<String, Object>) questMap.get("location"));
                        questList.put(id, new LocationQuest(title, reward, location, 3));
                    } catch (Exception e) {
                        plugin.getLogger().severe("Error at deserializing Location of quest " + id + ": " + e.getClass().getSimpleName() + " -> moving it to quests.yml/illegal-quests...");
                        newIllegalQuests.add(questMap);
                    }
                } else {
                    plugin.getLogger().severe("Error at quests.yml: " + questType + " is not a valid questType!");
                }
            }
            plugin.getLogger().info("...found " + questList.size() + " quests!");
            if (newIllegalQuests.size() > 0) {
                saveConfig(newIllegalQuests);
            }
        }
    }

    public void savePlayerDataConfig(FileConfiguration fileConfiguration) {
        plugin.getLogger().info("Saving player data to players.yml...");

        ConfigurationSection playersConfigurationSection = fileConfiguration.createSection("players");
        for (UUID uuid : playerList.keySet()) {
            playersConfigurationSection.set(uuid.toString(), playerList.get(uuid).serialize());
        }
    }

    public void saveQuestConfig(FileConfiguration fileConfiguration) {
        plugin.getLogger().info("Saving quests to quests.yml...");

        fileConfiguration.set("last-identifier", lastIdentifier);

        List<Map<?, ?>> questMapList = new ArrayList<>();
        for (Integer id : questList.keySet()) {
            Quest quest = questList.get(id);
            Map<String, Object> questMap = quest.serialize();
            questMap.put("id", id);
            questMapList.add(questMap);
        }
        fileConfiguration.set("quests", questMapList);

        if (__COMPAT_newIllegalQuests__ != null) {
            List<Map<?, ?>> illegalQuests = fileConfiguration.getMapList("illegal-quests");
            for (Map<?, ?> illegalQuestMap : __COMPAT_newIllegalQuests__) {
                illegalQuests.add(illegalQuestMap);
            }
            fileConfiguration.set("illegal-quests", illegalQuests);
            __COMPAT_newIllegalQuests__ = null;
        }
    }

    public boolean saveConfig(List<Map<?, ?>> newIllegalQuests) {
        System.out.println("sve");
        this.__COMPAT_newIllegalQuests__ = newIllegalQuests;
        return playerDataConfigurationLoader.save(true, false)
                & questsConfigurationLoader.save(true, false);
    }

    public Quest getQuest(Integer questIdentifier) {
        return questList.get(questIdentifier);
    }

    public QuestPlayer getQuestPlayer(OfflinePlayer player) {
        return getQuestPlayer(player.getUniqueId());
    }

    public QuestPlayer getQuestPlayer(UUID playerUniqueId) {
        QuestPlayer questPlayer = playerList.get(playerUniqueId);
        if (questPlayer == null) {
            questPlayer = new QuestPlayer(plugin, playerUniqueId);
            playerList.put(questPlayer.getUUID(), questPlayer);
        }
        return questPlayer;
    }

    public Integer addQuest(Quest quest) {
        questList.put(++lastIdentifier, quest);
        saveConfig(null);
        return lastIdentifier;
    }

    public void list(CommandSender receiver) {
        String[] messages;
        if (questList.size() > 0) {
            messages = new String[1 + (questList.size() * 4)];
            messages[0] = "----- Quest-list -----";
            int i = 1;
            for (Integer questIdentifier : questList.keySet()) {
                String[] questListStrings = questList.get(questIdentifier).asListStrings(questIdentifier);
                messages[i++] = questListStrings[0];
                messages[i++] = questListStrings[1];
                messages[i++] = questListStrings[2];
                messages[i++] = questListStrings[3];
            }
        } else {
            messages = new String[2];
            messages[0] = "----- Quest-list -----";
            messages[1] = "" + ChatUtil.VARIABLE + ChatColor.ITALIC + "(none)";
        }
        plugin.getChatUtil().send(receiver, messages, true);
    }

    public int getQuestIdentifier(Quest quest) {
        if (quest == null) throw new IllegalArgumentException("quest can not be null");
        for (Integer id : questList.keySet()) {
            if (questList.get(id).equals(quest)) return id;
        }
        return -1;
    }
}
