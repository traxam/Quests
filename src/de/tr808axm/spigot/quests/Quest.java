/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.quests;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.math.BigDecimal;

/**
 * Abstract class representing a quest. Instanced can only be created
 * Created by tr808axm on 10.04.2016.
 */
public abstract class Quest implements ConfigurationSerializable {
    public abstract String getTitle();

    public abstract BigDecimal getReward();

    public abstract boolean submit(QuestPlayer player);

    public abstract String[] asListStrings(int id);
}
