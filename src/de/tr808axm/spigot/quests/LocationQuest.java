/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.quests;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.pluginutils.UnlinkedLocation;
import org.bukkit.Location;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to store a "visit/travel-to"-Quest.
 * Created by tr808axm on 10.04.2016.
 */
public class LocationQuest extends Quest {
    private final String title;
    private final BigDecimal reward;
    private final Location location;
    private final int radius;

    public LocationQuest(String title, BigDecimal reward, Location location, int radius) {
        this.title = title;
        this.reward = reward;
        this.location = location;
        this.radius = radius;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public BigDecimal getReward() {
        return reward;
    }

    @Override
    public boolean submit(QuestPlayer player) {
        return location.getWorld().equals(player.getBukkitPlayer().getLocation().getWorld()) && location.distanceSquared(player.getBukkitPlayer().getLocation()) < radius;
    }

    @Override
    public String[] asListStrings(int id) {
        String[] listStrings = new String[4];
        listStrings[0] = "";
        listStrings[1] = ChatUtil.VARIABLE + getTitle();
        listStrings[2] = ChatUtil.DETAILS + "ID: " + id;
        listStrings[3] = ChatUtil.DETAILS + "Travel to: " + ChatUtil.formatLocation(UnlinkedLocation.fromLocation(location), true, false);
        return listStrings;
    }

    public Location getLocation() {
        return location;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> questMap = new HashMap<>();
        questMap.put("reward", reward.toString());
        questMap.put("title", title);
        questMap.put("type", "VISIT_LOCATION");
        Map<String, Object> locationMap = location.serialize();
        locationMap.remove("yaw");
        locationMap.remove("pitch");
        questMap.put("location", locationMap);
        return questMap;
    }
}
