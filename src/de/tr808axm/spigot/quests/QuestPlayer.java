/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.quests;

import com.sun.istack.internal.NotNull;
import de.tr808axm.spigot.economy.api.EconomyAPI;
import de.tr808axm.spigot.economy.api.EconomyOperationData;
import de.tr808axm.spigot.pluginutils.ChatUtil;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Used to store a questing player.
 * Created by tr808axm on 30.04.2016.
 */
public class QuestPlayer implements ConfigurationSerializable {
    private final QuestsMain plugin;
    private final UUID uuid;
    private final Quest[] activeQuests;
    private Quest[] doneQuests;

    public QuestPlayer(QuestsMain plugin, OfflinePlayer player) {
        this(plugin, player.getUniqueId());
    }

    public QuestPlayer(QuestsMain plugin, UUID uuid) {
        this(plugin, uuid, null, null);
    }

    public QuestPlayer(QuestsMain plugin, UUID uuid, Quest[] activeQuests, Quest[] doneQuests) {
        this.plugin = plugin;
        this.uuid = uuid;
        this.activeQuests = activeQuests != null ? activeQuests : new Quest[3];
        this.doneQuests = doneQuests != null ? doneQuests : new Quest[0];
    }

    public boolean takeQuest(Quest quest) {
        if (!hasQuest(quest) && !hasDoneQuest(quest) && !isMaximumReached()) {
            for (int i = 0; i < activeQuests.length; i++) {
                if (activeQuests[i] == null) {
                    activeQuests[i] = quest;
                    return true;
                }
            }
        }
        return false;
    }

    private boolean hasDoneQuest(@NotNull Quest quest) {

        if (quest == null) throw new IllegalArgumentException("Quest may not be null!");
        for (Quest doneQuest : doneQuests) {
            if (quest.equals(doneQuest)) return true;
        }
        return false;
    }

    public boolean hasQuest(@NotNull Quest quest) {
        if (quest == null) throw new IllegalArgumentException("Quest may not be null!");
        for (Quest activeQuest : activeQuests) {
            if (quest.equals(activeQuest)) return true;
        }
        return false;
    }

    public boolean isMaximumReached() {
        for (Quest activeQuest : activeQuests) {
            if (activeQuest == null) return false;
        }
        return true;
    }

    public Quest[] getActiveQuests() {
        return activeQuests.clone();
    }

    public Quest[] getDoneQuests() {
        return doneQuests.clone();
    }

    public UUID getUUID() {
        return uuid;
    }

    // TODO? rename
    public boolean finishQuest(Quest quest) {
        if (hasQuest(quest)) {
            for (int i = 0; i < activeQuests.length; i++)
                if (quest.equals(activeQuests[i])) {
                    activeQuests[i] = null;
                    EconomyAPI.add(new EconomyOperationData(uuid, quest.getReward()), true);
                    plugin.getChatUtil().send(getBukkitPlayer(), "You completed '" + ChatUtil.VARIABLE + quest.getTitle() + ChatUtil.POSITIVE + "'!", true);
                    Quest[] newDoneQuests = new Quest[doneQuests.length + 1];
                    newDoneQuests[doneQuests.length] = quest;
                    System.arraycopy(doneQuests, 0, newDoneQuests, 0, doneQuests.length);
                    doneQuests = newDoneQuests;
                    return true;
                }
        }
        return false;
    }

    public int getActiveQuestCount() { //TODO? store this number and recalculate it only when questList updates
        int activeQuestCount = 0;
        for (Quest activeQuest : activeQuests) {
            if (activeQuest != null) activeQuestCount++;
        }
        return activeQuestCount;
    }

    public boolean submit(Quest quest) {
        if (hasQuest(quest) && quest.submit(this)) {
            finishQuest(quest); //TODO move?
            return true;
        }
        return false;
    }

    public Player getBukkitPlayer() {
        return plugin.getServer().getPlayer(uuid);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> questPlayerMap = new HashMap<>();
        {
            List<Integer> activeQuestsList = new ArrayList<>();
            for (Quest activeQuest : activeQuests) {
                if (activeQuest != null) activeQuestsList.add(plugin.getQuestManager().getQuestIdentifier(activeQuest));
            }
            questPlayerMap.put("active-quests", activeQuestsList);
        }
        {
            List<Integer> doneQuestList = new ArrayList<>();
            for (Quest doneQuest : doneQuests) {
                doneQuestList.add(plugin.getQuestManager().getQuestIdentifier(doneQuest));
            }
            questPlayerMap.put("done-quests", doneQuestList);
        }
        return questPlayerMap;
    }
}
