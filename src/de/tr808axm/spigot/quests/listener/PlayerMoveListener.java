/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.quests.listener;

import de.tr808axm.spigot.quests.LocationQuest;
import de.tr808axm.spigot.quests.Quest;
import de.tr808axm.spigot.quests.QuestPlayer;
import de.tr808axm.spigot.quests.QuestsMain;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Used to catch the player visiting it's
 * Created by tr808axm on 01.05.2016.
 */
public class PlayerMoveListener implements Listener {
    private final QuestsMain plugin;

    public PlayerMoveListener(QuestsMain plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        QuestPlayer questPlayer = plugin.getQuestManager().getQuestPlayer(event.getPlayer());
        for (Quest quest : questPlayer.getActiveQuests()) {
            if (quest != null && quest instanceof LocationQuest) {
                questPlayer.submit(quest);
            }
        }
    }
}
