/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.quests;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.quests.command.QuestsCommandExecutor;
import de.tr808axm.spigot.quests.listener.PlayerMoveListener;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

/**
 * Quests-plugin's main class.
 * Created by tr808axm on 30.11.2016.
 */
public class QuestsMain extends JavaPlugin {
    private ChatUtil chatUtil;
    private QuestManager questManager;

    @Override
    public void onLoad() {
        super.onLoad();
        chatUtil = new ChatUtil(getDescription(), ChatColor.BLUE, getServer());
        getLogger().info("Loaded!");
    }

    @Override
    public void onEnable() {
        super.onEnable();
        registerCommands();
        registerEvents();
        try {
            questManager = new QuestManager(this);
        } catch (IOException e) {
            getLogger().info("Fatal error at loading config storage files: " + e.getClass().getSimpleName() + ": " + e.getMessage());
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        getLogger().info("Enabled!");
    }

    private void registerCommands() {
        getCommand("quests").setExecutor(new QuestsCommandExecutor(this));
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new PlayerMoveListener(this), this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
        if (questManager != null && !questManager.saveConfig(null)) getLogger().severe("Could not save data.");
        getLogger().info("Disabled!");
    }

    public QuestManager getQuestManager() {
        return questManager;
    }

    public ChatUtil getChatUtil() {
        return chatUtil;
    }
}
